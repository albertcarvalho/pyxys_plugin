<?php

namespace PYXYS\Core;

if (!defined('PYXYS_PLUGIN_VERSION')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class Activation {

	public static function plugin_activation() {
	}
		
	public static function plugin_deactivation() {
	}

	public static function plugin_uninstall() {
	}
}