<?php

namespace PYXYS\Core;

use PYXYS\Helpers AS Helpers;

if (!defined('PYXYS_PLUGIN_VERSION')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class PyxysPlugin {
		
	public static function init() {
		
		add_action('admin_menu', array(__CLASS__, 'add_menu_page'));
		add_action('admin_enqueue_scripts', array(__CLASS__, 'enqueue_admin_scripts'));
	}
	
	public static function enqueue_admin_scripts() {
		
		wp_register_style('pyxys-admin-style', PYXYS_PLUGIN_URL . 'includes/admin/css/style.css?v=5', null, 1);
		wp_enqueue_style('pyxys-admin-style');
		
		wp_enqueue_media();
		wp_enqueue_script('pyxys-uploader-js', PYXYS_PLUGIN_URL . 'includes/admin/js/uploader.js?v=5', array( 'jquery'), '', true  );
	}
	
	public static function add_menu_page()
	{
		add_menu_page(
			'pyxyspm_settings_page',
			'PYXYS Plugin',
			'manage_options',
			'pyxyspm_settings_menu',
			array(__CLASS__, 'show_plugin_page'),
			PYXYS_PLUGIN_URL . 'includes/admin/images/icon.png',
			20
		);
	}

	public static function show_plugin_page() {

		$page_data = Helpers\DataHelper::get_post_page_data();
		$page_id = 0;
		$errors = array();
		
		if(isset($_POST['action']) && $_POST['action'] == 'submit_page_data') {
			
			$errors = Helpers\DataHelper::validate_page_data($page_data);
			
			if(empty($errors)) {
				
				$page_id = Helpers\DataHelper::save_page_data($page_data);
				
				if($page_id == 0) {
					$errors['save'] = 'Falha ao salvar';
				}
			}
		}
		
		if($page_id == 0) {
			
			Helpers\PageFormHtmlHelper::print_page_data_menu($page_data, $errors);
		}
		else {
			
			Helpers\PageFormHtmlHelper::print_page_saved($page_id);
		}
		
	}
}