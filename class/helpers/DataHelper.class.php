<?php

namespace PYXYS\Helpers;

if (!defined('PYXYS_PLUGIN_VERSION')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class DataHelper {
	
	public static function get_post_page_data() {
		
		return array(
		  'post_title'    => sanitize_text_field($_POST['title']),
		  'post_content'  => $_POST['content'],
		  'post_excerpt' => sanitize_text_field($_POST['excerpt']),
		  'post_thumbnail' => (int)$_POST['image']
		);
	}
	
	public static function validate_page_data($page_data) {
		
		$errors = array();
		
		if(empty($page_data['post_thumbnail'])) {
			$errors['post_thumbnail'] = 'O campo <b>Imagem</b> é obrigatório';
		}
		
		if(empty($page_data['post_title'])) {
			$errors['post_title'] = 'O campo <b>Título</b> é obrigatório';
		}
		
		if(empty($page_data['post_content'])) {
			$errors['post_content'] = 'O campo <b>Conteúdo</b> é obrigatório';
		}
		
		if(empty($page_data['post_excerpt'])) {
			$errors['post_excerpt'] = 'O campo <b>Resumo</b> é obrigatório';
		}
		
		return $errors;
	}
	
	public static function save_page_data($page_data){
		
		$values = array(
			'post_title' => $page_data['post_title'],
			'post_content' => $page_data['post_content'],
			'post_excerpt' => $page_data['post_excerpt'],
			'post_status' => 'publish',
			'post_type' => 'page'
		);
		
		$page_id = wp_insert_post( $values );
		
		if($page_id == 0) return 0;
		
		if($page_data['post_thumbnail'] > 0) {
			
			set_post_thumbnail( $page_id, $page_data['post_thumbnail'] );
		}
		
		return $page_id;
	}
}