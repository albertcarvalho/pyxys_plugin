<?php

namespace PYXYS\Helpers;

if (!defined('PYXYS_PLUGIN_VERSION')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class PageFormHtmlHelper {
	
	public static function print_content_field($content) {
		
		$settings = array(
			'teeny' => true,
			'textarea_rows' => 15,
			'media_buttons' => false,
			'textarea_name' => 'content'
		);
		wp_editor($content, 'page_content_wditor', $settings);
	}
	
	public static function print_image_field($image_id) { 
	
		$image_attributes = wp_get_attachment_image_src($image_id);
		$src = $image_attributes[0];
	
	?>
		<div>
			<img src="<?=$src?>"/>
			<div>
				<input type="hidden" name="image" value="<?=$image_id?>"/>
				<button  id="upload_image_button" class="btn btn-primary">Alterar Imagem</button>
			</div>
		</div>
	<?php }
	
	public static function print_title_field($content = '', $label = 'Título') { ?>
	
			<div id="titlediv">
				<div id="titlewrap">
					<label class="" id="title-prompt-text" for="title"><?=$label?></label>
					<input type="text" name="title" size="30" value="<?=$content?>" id="title" spellcheck="true" autocomplete="off">
				</div>
				<div class="inside">
					<div id="edit-slug-box" class="hide-if-no-js"></div>
				</div>
			</div>
	<?php }
	
	public static function print_excerpt_field($content = '', $label = 'Resumo') { ?>
			<div class="postbox">
				<h1><?=$label?></h1>
				<div class="inside">
					<label class="screen-reader-text" for="excerpt"><?=$label?></label>
					<textarea rows="1" cols="40" name="excerpt" id="excerpt"><?=$content?></textarea>
				</div>
			</div>
	<?php }
	
	public static function print_page_data_menu($page_data, $errors = array()) { ?>
		<div class="wrap">
			<img height="70px" style="margin-bottom:10px;" src="<?=PYXYS_PLUGIN_URL . 'includes/admin/images/logo.jpg'?>"/>
			<form method="post">
				<input type="hidden" name="action" value="submit_page_data">
			
			<?php
				self::print_title_field($page_data['post_title']);
				self::print_content_field($page_data['post_content']);
				self::print_excerpt_field($page_data['post_excerpt']);
				self::print_image_field($page_data['post_thumbnail']);
				submit_button('Salvar');
			?>
			
			</form>
			<?php self::_print_errors($errors); ?>
		</div>
	
	<?php }
	
	public static function print_page_saved($page_id) { ?>
	
		<div class="wrap">
			<h1>Página salva com sucesso</h1>
		</div>
	<?php }
	
	private static function _print_errors($errors) {
		
		foreach($errors as $key => $value) {
			echo '<div>' . $value . '</div>';
		}
	}
}