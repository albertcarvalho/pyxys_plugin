<?php
/*
Plugin Name:  PYXYS Plugin
Plugin URI:   
Description:  Plugin desenvolvido para o desafio prático Wordpress PYXYS
Version:      1.0.0
Author:       Roni Albert A. S. Carvalho
Author MAIL:  ronialbert@hotmail.com
*/

namespace PYXYS;

if (!function_exists('add_action')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define( __NAMESPACE__ . '\PYXYS', __NAMESPACE__ . '\\' );

define( 'PYXYS_PLUGIN_VERSION', '1.0.0' );
define( 'PYXYS_PLUGIN_URL', plugin_dir_url(__FILE__));
define( 'PYXYS_PLUGIN_DIR', plugin_dir_path(__FILE__) );

spl_autoload_register(__NAMESPACE__ . '\\autoload');
function autoload($cls)
{
    $cls = ltrim($cls, '\\');
    if(strpos($cls, __NAMESPACE__) !== 0)
        return;

    $cls = str_replace(__NAMESPACE__, '', $cls);

    $path = PYXYS_PLUGIN_DIR . 'class' . 
        str_replace('\\', DIRECTORY_SEPARATOR, $cls) . '.class.php';

    require_once($path);
}

register_activation_hook( __FILE__ , array('PYXYS\Core\Activation', 'plugin_activation'));
register_deactivation_hook( __FILE__ , array('PYXYS\Core\Activation', 'plugin_deactivation'));
register_uninstall_hook( __FILE__ , array('PYXYS\Core\Activation', 'plugin_uninstall'));

add_action('init', array('PYXYS\Core\PyxysPlugin', 'init'));